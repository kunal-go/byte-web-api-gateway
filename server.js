const express = require("express")
const bodyParser = require("body-parser")
const cors = require('cors')
const helmet = require('helmet')

// Server Function
function start(options = {}) {
    // Option: Parser Limit
    let parserLimit = options.parserLimit || '100kb'

    // Option: CORS
    // Defaults
    let corsOptions = {
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        preflightContinue: false,
        optionsSuccessStatus: 204
    }


    if (options.cors) {
        if (options.cors.origin) {
            corsOptions.origin = options.cors.origin
        }
        if (options.cors.methods) {
            corsOptions.methods = options.cors.methods
        }
        if (options.cors.allowedHeaders) {
            corsOptions.allowedHeaders = options.cors.allowedHeaders
        }
        if (options.cors.exposedHeaders) {
            corsOptions.exposedHeaders = options.cors.exposedHeaders
        }
        if (options.cors.credentials) {
            corsOptions.credentials = options.cors.credentials
        }
        if (options.cors.maxAge) {
            corsOptions.maxAge = options.cors.maxAge
        }
        if (options.cors.preflightContinue) {
            corsOptions.preflightContinue = options.cors.preflightContinue
        }
        if (options.cors.optionsSuccessStatus) {
            corsOptions.optionsSuccessStatus = options.cors.optionsSuccessStatus
        }
    }


    // Initializing
    const app = express()

    // Helmet : Request Header Protection
    app.use(helmet())

    // CORS : Allowing Cross Domian Access
    app.use(cors(corsOptions))

    // Request Body Parser
    app.use(bodyParser.json({ limit: parserLimit }))
    app.use(bodyParser.urlencoded({ limit: parserLimit, extended: true }))

    // Returning
    return app
}

// Exporting
module.exports = {
    start
}

