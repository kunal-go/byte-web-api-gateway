class Response {
	constructor(res, status = 404, success = false, data = undefined, error = undefined) {
		this.status = status
		this.success = success
		this.data = data
		this.error = error
		this.sendResponse(res)
	}

	// Sending Response
	sendResponse(res) {
		res.status(this.status).json({
			success: this.success,
			data: this.data,
			error: this.error
		})
	}
}

// Error Response
function error(res, err, showStack = false) {
	new Response(res, err.httpStatus || 500, false, undefined, {
		name: err.name,
		message: err.message,
		stack: showStack ? err.stack : undefined
	})
}

// Success Created Response
function created(res, created = undefined) {
	new Response(res, 201, true, {
		created
	})
}

// Success Response
function success(res, data = undefined) {
	new Response(res, 200, true, data)

}

module.exports = {
	error,
	created,
	success
}