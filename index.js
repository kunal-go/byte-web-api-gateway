module.exports = {
    express: require("express"),
    server: require("./server"),
    responses: require("./responses")
}